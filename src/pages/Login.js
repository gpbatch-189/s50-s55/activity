import { useState, useEffect, useContext } from 'react';
import {Form, Button} from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {

	//Allows us to consume the user context object and its properties to use for user validation
	const { user, setUser} = useContext(UserContext)

	//State hooks to store the values of the input fields
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// console.log(email)

	function authenticate(e) {

		e.preventDefault();

		/*
		Syntax:
			fetch('url', {options})
			.then(res => res.json())
			.then( data => {})
		
		*/

		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password 
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			// data is the access token

			if(typeof data.access !== "undefined") {
									//token here is user defined	
				localStorage.setItem('token', data.access)

				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome to Zuitt!"
				})
			} else {

				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Check your login details and try again!"
				})

			}

		})




		//Set the email of the authenticated user in the local storage
		/*
			Syntax:
				localStorage.setItem("propertyName", value)
		*/
							// key    value from const sa taas	
		// localStorage.setItem("email", email);

		//Set the global user state to have properties obtained from local storage
		// setUser({
		// 	email: localStorage.getItem('email')
		// })

		setEmail("");
		setPassword("");
	

		// alert ("You are now logged in!");

	}


	const retrieveUserDetails = (token) => {

		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})

	}


	useEffect(() => {

		if(email !== "" && password !== "") {

			setIsActive(true)
		} else {
			setIsActive(false)
		}

	}, [email, password])

	return(

	(user.id !== null) ?
		<Navigate to ="/courses" />

		:

		<Form className="mt-3" onSubmit={(e) => authenticate(e)}>
			<h1 className="text-center">Login</h1>
	      <Form.Group className="mb-3" controlId="userEmail">
	        <Form.Label>Email address</Form.Label>
	        <Form.Control 
	        		type="email" 
	        		placeholder="Enter email"
	        		value={email}
	        		onChange={e => {
	        			setEmail(e.target.value) // "" yung initial value ng email, this sets a new value sa email 
	        			// console.log(e.target.value)
	        		}}
	        		required />
	        
	      </Form.Group>

	      <Form.Group className="mb-3" controlId="password1">
	        <Form.Label>Password</Form.Label>
	        <Form.Control 
	        	type="password" 
	        	placeholder="Password"
	        	value={password}
	        	onChange={e => setPassword(e.target.value)}	
	        	required />
	      </Form.Group>

	       {
	    		isActive ?
		    		<Button variant="primary" type="submit" id="submitBtn">
			        Submit
			     	</Button>
			     	:
			     	<Button variant="danger" type="submit" id="submitBtn" disabled>
			        Submit
			     	</Button>			     
	    	}
	      
	    </Form>

	   

		)
}