import { useState, useEffect } from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function CourseCard({courseProp}) {
	
	// console.log(props)
	// console.log(typeof props)
	// console.log(props.courseProp.name)
	// console.log(courseProp)

	/*

		Use the state hook for this component to be able to store its state.
		States are used to keep track of information related to individual components

		Syntax:
			const [getter, setter] = useState(initialGetterValue)
	
	*/

	// setCount : nags set kung ano yung magiging value ng count natin
	// kaya 0 yung initial value sa console log kasi asynchronous yung setCount function. Hindi pa na process yung +1, na console log na.


	// const [count, setCount] = useState(0)

	// function enroll() {
	
	// 	setCount(count + 1);
	// 	console.log('Enrollees: ' + count)
		
	// 	if (count >= 30) {
	// 		return (alert("No More Seats"))
	// 		}
	// }	

	// const [count, setCount] = useState(0)
	// const [seats, setSeats] = useState(30)

	// function enroll() {

	// 	// if (seats > 0) {
	// 		setCount(count + 1);
	// 		console.log('Enrollees: ' + count);
	// 		setSeats(seats - 1);
	// 		console.log('Seats: ' + seats);
			
	// 	// } else 	{
	// 	// 	alert("No More Seats")
	// 	// };


	// }	


	// useEffect(() => {
	// 	if (seats === 0) {
	// 		alert('No more seats available')
	// 	}
	// }, [seats])	
	// kaya merong [seats] dito sa last para kung magbago lang ung value ng seats tska lang tatawagin yung nasa useEffect
	// kapag empty array lang yung nasa seats, isang beses lang nya tatawagin yung function





	// const [count, setCount] = useState(0)

	// function seats() {
	// 	setCount(count - 1);
	// 	console.log('Enrollees: ' + count)
	
	// 	if (count === 0) {
	// 		return (alert("No More Seats"))
	// 		}
	// }	


	//Deconstruct the course properties into their own variables
	// Upper case na yung C sa onClick

	const { name, description, price, _id } = courseProp;
	console.log(courseProp)

	return (

		<Card>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PhP {price}</Card.Text>
				<Button variant="primary" as={Link} to={`/courses/${_id}`}>Details</Button>	

			</Card.Body>
		</Card>
			    
    );
}

/*

	<Link className="btn btn-primary" to="courseView">Details</Link>

*/
