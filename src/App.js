// import {Fragment} from 'react';
import {useState, useEffect} from 'react';
import {Container} from 'react-bootstrap';
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import CourseView from './pages/CourseView'
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import PageNotFound from './pages/PageNotFound';
import './App.css';
import { UserProvider } from './UserContext';


function App() {

  // State hook for the user state that's defined here for a global scope 
  const[user, setUser] = useState({
    // email: localStorage.getItem('email')
    id: null,
    isAdmin: null
  })

  //Function for clearing localStorage on logout
  const unsetUser = () => {
    localStorage.clear();
  }


  //
  useEffect(() => {
    fetch('http://localhost:4000/users/details', {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(res => res.json())
    .then(data => {
        if(typeof data.id !== "undefined") {

          setUser({
            id: data._id,
            isAdmin: data.isAdmin           
          })

        } else {

          setUser({
            id: null,
            isAdmin: null
          })
  
        }
    })


  }, [])
  // kapag walang empty array, tuloy tuloy lang sya magl load.

  return (
      <UserProvider value={{user, setUser, unsetUser}}>
        <Router> 
          <AppNavbar/>
          <Container>
          <Routes> // responsible para mag switch kung ano yung il-load sa page
            <Route path="/" element={<Home/>} />
            <Route path="/courses" element={<Courses/>} />
            <Route path="/courses/:courseId" element={<CourseView/>} />
            <Route path="/login" element={<Login/>} />
            <Route path="/logout" element={<Logout />} />
            <Route path="/register" element={<Register/>} />
            <Route path="*" element={<PageNotFound />} />
          </Routes>
          </Container>
        </Router>
      </UserProvider>

  );
}




export default App;
